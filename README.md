Events
======

Facilitates communication between loosely coupled components using multicast publish/subscribe mechanism.

This library is based on the new Microsoft.Practices.Prism.PubSubEvents library release by Microsoft. Among others, [Prismactic.Events](https://github.com/Prismactic/Events) is a Portable Class Library that supports .NET 4.0 and higher, Xamarin.iOS, Xamarin.Android, Xamarin.Mac, Windows Store apps 8 and higher, Sliverlight 5, and Windows Phone 8.
